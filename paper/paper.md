---
title: 'inu: Inertial Navigation Utilities'
tags:
  - Python
  - inertial navigation
  - extended Kalman filter
  - accelerometers
  - gyroscopes
authors:
  - name: David Woodburn
    orcid: 0009-0000-5935-2850
    equal-contrib: false
    affiliation: 1
affiliations:
 - name: Air Force Institute of Technology, USA
   index: 1
date: 03 October 2024
bibliography: paper.bib
---

# Summary

This library provides forward mechanization of inertial measurement unit sensor
values (accelerometer and gyroscope readings) to get position, velocity, and
attitude as well as inverse mechanization to get sensor values from position,
velocity, and attitude. It also includes tools to calculate velocity from
geodetic position over time, to estimate attitude from velocity, and to estimate
wind velocity from ground-track velocity and yaw angle.

The mechanization algorithms in this library make no simplifying assumptions.
The Earth is defined as an ellipsoid. Any deviations of the truth from this
simple shape can be captured by more complex gravity models. The algorithms use
a single frequency update structure which is much simpler than the common
two-frequency update structure and just as, if not more, accurate.

The forward and inverse mechanization functions are perfect duals of each other.
This means that if you started with a profile of position, velocity, and
attitude and passed these into the inverse mechanization algorithm to get sensor
values and then passed those sensor values into the forward mechanization
algorithm, you would get back the original position, velocity, and attitude
profiles. The only error would be due to finite-precision rounding. This is a
rarely-implemented feature, yet it makes generating realistic sensor readings
from position, velocity, and attitude possible.

This library also include the Jacobian required to produce an extended Kalman
filter with whatever other sensors the user wishes.

# References
