# CHANGELOG

## 1.1.6 (2024-11-13)

### Fixed

*   Added distribution statement to LICENSE file.

## 1.1.5 (2024-11-13)

### Fixed

*   Added automatic unwrapping of attitude angles to inv_mech function.

## 1.1.4 (2024-10-03)

### New

*   Draft paper for Journal of Open Source Software

## 1.1.3 (2024-09-09)

### Fixed

*   Missing submodule for norm.

## 1.1.2 (2024-09-09)

### New

*   Made vne_to_rpy function handle 2D grav_t inputs.
*   Added 0.06 radian default for alpha to vne_to_rpy.
*   Added rotation to the Q matrix in the EKF example.

## 1.1.1 (2024-08-19)

### Fixed

*   Removed metadata from `inu.py` in favor of `pyproject.toml` metadata.

## 1.1.0 (2024-05-31)

### New

*   `inv_mech()` no longer requires `vne_t` as an input.
*   `ned_enu()` can no handle a single vector as well as a matrix of vectors in
    either orientation.

### Fixed

*   Multiple functions now also check for tuples, not just lists.
